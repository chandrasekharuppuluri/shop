from django.urls import path
from kart.views import ProductsList, ProductDetails,OrdersList,OrdersDetails, OrderItemsList,OrderItemsDetails

urlpatterns = [
    path('products/', ProductsList.as_view({'get': 'list'}), name='productslist'),
    path('product/<pk>',
         ProductDetails.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='productsdetails'),
    path('orders/', OrdersList.as_view({'get': 'list'}), name='listoforders'),
    path('order/<str:pk>',
         OrdersDetails.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='ordersdetails'),
    path('orderproduct/', OrderItemsList.as_view({'get': 'list'}), name='orderitemslist'),
    path('orderproduct/<str:pk>',
         OrderItemsDetails.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='orderitemslist'),
]