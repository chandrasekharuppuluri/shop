from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Product(models.Model):
    Title = models.CharField(max_length=50)
    Description = models.TextField()
    ImageLink = models.ImageField()
    Price = models.FloatField()
    created_on = models.DateField(auto_now_add=True)
    updated_on = models.DateField(auto_now=True)

    def __str__(self):
        return self.Title


STATUS_CHOICES = (
    ('new', 'new'),
    ('paid', 'paid')
)

MODE_OF_PAYMENT = (
    ('cash_on_delivery', 'paytm'),
)


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    Total = models.IntegerField()
    Order_placed_on = models.DateField(auto_now_add=True)
    Order_updated_on = models.DateField(auto_now=True)
    Status = models.CharField(choices=STATUS_CHOICES, max_length=5)
    Mode_of_payment = models.CharField(choices=MODE_OF_PAYMENT, max_length=25)
    Product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return self.Product.Title


class OrderItem(models.Model):
    Order_Id = models.ForeignKey(Order, on_delete=models.CASCADE)
    Product_Id = models.ForeignKey(Product, on_delete=models.CASCADE)
    Quantity_of_product = models.IntegerField()
    Price = models.FloatField()
    user= models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.Product_Id.Title