from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from rest_framework.filters import SearchFilter,OrderingFilter
from kart.models import Product, Order, OrderItem
from kart.serializers import ProductSerializer, OrderSerializer, OrderItemSerializer


class ProductsList(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    search_fields = ['id', 'Title']
    filter_backends = (SearchFilter, OrderingFilter)

class ProductDetails(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class IsAuthorized(permissions.BasePermission):

    def has_object_permission(self, request, view, object):
        return object.User == request.User

class OrdersList(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Order.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OrdersDetails(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Order.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class OrderItemsList(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderItemSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderItem.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OrderItemsDetails(viewsets.ModelViewSet):
    serializer_class = OrderItemSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderItem.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
