from django.contrib import admin

# Register your models here.
from kart.models import Product, OrderItem, Order

admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderItem)