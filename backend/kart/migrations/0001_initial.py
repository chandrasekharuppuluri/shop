# Generated by Django 3.1.5 on 2021-01-07 17:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Total', models.IntegerField()),
                ('Order_placed_on', models.DateField(auto_now_add=True)),
                ('Order_updated_on', models.DateField(auto_now=True)),
                ('Status', models.CharField(choices=[('new', 'new'), ('paid', 'paid')], max_length=5)),
                ('Mode_of_payment', models.CharField(choices=[('cash_on_delivery', 'paytm')], max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Title', models.CharField(max_length=50)),
                ('Description', models.TextField()),
                ('ImageLink', models.ImageField(upload_to='')),
                ('Price', models.FloatField()),
                ('created_on', models.DateField(auto_now_add=True)),
                ('updated_on', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Quantity_of_product', models.IntegerField()),
                ('Price', models.FloatField()),
                ('Order_Id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kart.order')),
                ('Product_Id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kart.product')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='Product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kart.product'),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
